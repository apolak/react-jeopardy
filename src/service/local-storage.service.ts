import { JoepardyState } from '../model/domain';

export const saveState: Function = (state: JoepardyState): void => {
	localStorage.setItem('react-jeopardy', JSON.stringify(state));
};

export const loadState: Function = (): JoepardyState => {
	return JSON.parse(localStorage.getItem('react-jeopardy'));
};
