import { connect } from 'react-redux';
import { JoepardyState, Question } from '../../model/domain';
import { ActiveQuestionComponent } from './active-question.component';
import { createNextQuestionAction, createSkipQuestionAction, createAnswerQuestionAction, createFailQuestionAction } from '../../model/actions';

const mapStateToProps: any = (state: JoepardyState): Object => {
	return {
		users: state.users,
		question: state.activeQuestion
	}
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		onNextQuestion: () => {
			dispatch(createNextQuestionAction())
		},
		onSkipQuestion: (id: number) => {
			dispatch(createSkipQuestionAction(id))
		},
		onFailQuestion: (question: Question, userId: number) => {
			dispatch(createFailQuestionAction(question, userId))
		},
		onAnswerQuestion: (question: Question, userId: number) => {
			dispatch(createAnswerQuestionAction(question, userId))
		}
	}
};

export const ConnectedActiveQuestionComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(ActiveQuestionComponent);
