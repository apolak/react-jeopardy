import * as React from 'react';
import { Question, User } from '../../model/domain';

import "./active-question.component.styl";

type ActiveQuestionComponentProps = {
	question: Question,
	users: User[],
	onNextQuestion: Function,
	onSkipQuestion: Function,
	onAnswerQuestion: Function,
	onFailQuestion: Function
}

export class ActiveQuestionComponent extends React.Component<ActiveQuestionComponentProps, void> {
	render(): JSX.Element {
		if (this.props.question) {
			if (this.props.question.answered) {
				return (
					<div className="active-question">
						<div className="question" onClick={() => this.props.onNextQuestion()}>
							<p>{this.props.question.answer}</p>
						</div>
					</div>
				)
			}

			return (
				<div className="active-question">
					<div className="question">
						<p>{this.props.question.question}</p>
					</div>
					<div className="users">
						{
							this.props.users.map((user: User) => {
								return (
									<div key={user.id} className="user" onClick={ (event) => this.handleAnswer(event, user)}>
										<p>{user.name}</p>
										<p>{user.score}</p>
									</div>
								)
							})
						}
						<div className="skip" onClick={() => this.props.onSkipQuestion(this.props.question.id)}>
							<p>Skip</p>
						</div>
					</div>
				</div>
			)
		}

		return null;
	}

	private handleAnswer(event: any, user: User): any {
		if (!event.shiftKey) {
			this.props.onAnswerQuestion(this.props.question, user.id);
		} else {
			this.props.onFailQuestion(this.props.question, user.id);
		}
	}
}
