import * as React from 'react';
import { mount } from "enzyme";
import { expect } from "chai";
import * as sinon from "sinon";

import { QuestionComponent } from "./question.component";

describe('Question Component', () => {
	it('should display not answered question body', () => {
		const props = {
			question: {
				id: 1,
				price: 100,
				question: 'some-question',
				answer: 'some-answer',
				answered: false,
				categoryId: 1
			},
			onQuestionSelect: () => {}
		};

		const wrapper = mount(<QuestionComponent {...props} />);
		const body = wrapper.find('p');

		expect(body).to.have.text(props.question.price.toString());
	});

	it('should display answered question body', () => {
		const props = {
			question: {
				id: 1,
				price: 100,
				question: 'some-question',
				answer: 'some-answer',
				answered: true,
				categoryId: 1
			},
			onQuestionSelect: () => {}
		};

		const wrapper = mount(<QuestionComponent {...props} />);
		const body = wrapper.find('p');

		expect(body).to.have.className('answered');
	});

	it('should dispatch action when clicking on not answered question', () => {
		const props = {
			question: {
				id: 1,
				price: 100,
				question: 'some-question',
				answer: 'some-answer',
				answered: false,
				categoryId: 1
			},
			onQuestionSelect: sinon.spy()
		};

		const wrapper = mount(<QuestionComponent {...props} />);
		wrapper.find('.question').simulate('click');

		expect(props.onQuestionSelect.calledOnce).to.equal(true);
	});

	it('should not dispatch action when clicking on answered question', () => {
		const props = {
			question: {
				id: 1,
				price: 100,
				question: 'some-question',
				answer: 'some-answer',
				answered: true,
				categoryId: 1
			},
			onQuestionSelect: sinon.spy()
		};

		const wrapper = mount(<QuestionComponent {...props} />);
		wrapper.find('.question').simulate('click');

		expect(props.onQuestionSelect.notCalled).to.equal(true);
	})
});
