import * as React from 'react';
import { GameCategory } from '../board.component';
import { Question } from '../../../model/domain';
import { QuestionComponent } from '../question/question.component';

type CategoryComponentProps = {
	category: GameCategory,
	onQuestionSelect: Function
}

export class CategoryComponent extends React.Component<CategoryComponentProps, void> {
	render(): JSX.Element {
		return (
			<div className="category-container">
				<div className="category">{this.props.category.name}</div>
				{
					this.props.category.questions.map((question: Question) => {
						return <QuestionComponent
							key={question.id}
							question={question}
						    onQuestionSelect={this.props.onQuestionSelect}
						/>
					})
				}
			</div>
		)
	}
}
