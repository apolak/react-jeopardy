import { connect } from 'react-redux';
import { JoepardyState, Category, Question } from '../../model/domain';
import {
	createAddCategoryAction, createRemoveCategoryAction, createAddQuestionAction,
	createRemoveQuestionAction, createSelectQuestionAction
} from '../../model/actions';
import { BoardComponent, GameCategory } from './board.component';

const mapStateToProps: any = (state: JoepardyState): Object => {
	return {
		users: state.users,
		categories: state.categories.map((category: Category): GameCategory => {
			return {
				id: category.id,
				name: category.name,
				questions: state.questions.filter((question: Question): boolean => {
					return question.categoryId === category.id;
				})
			}
		})
	}
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		onQuestionSelect: (question: Question) => {
			dispatch(createSelectQuestionAction(question))
		}
	}
};

export const ConnectedBoardComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(BoardComponent);
