import * as React from 'react';
import { User, Question } from '../../model/domain';
import { CategoryComponent } from './category/category.component';

import "./board.component.styl";

export type GameCategory = {
	id: number,
	name: string,
	questions: Question[]
}

type BoardComponentProps = {
	users: User[],
	categories: GameCategory[],
	onQuestionSelect: Function
}

export class BoardComponent extends React.Component<BoardComponentProps, void> {
	render(): JSX.Element {
		return (
			<div className="board">
				<div className="categories">
					{
						this.props.categories.map((gameCategory: GameCategory) => {
							return <CategoryComponent
								key={gameCategory.id}
								category={gameCategory}
							    onQuestionSelect={this.props.onQuestionSelect}
							/>
						})
					}
				</div>
				<div className="users">
					{
						this.props.users.map((user: User) => {
							return <div className="user" key={user.id}>
								<p>{user.name}</p>
								<p>{user.score}</p>
							</div>
						})
					}
				</div>
			</div>
		)
	}
}
