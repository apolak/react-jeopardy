import * as React from 'react';
import { User } from '../../model/domain';
import { AddUserComponent } from './add-user/add-user.component';
import { UserComponent } from './user/user.component';

type UsersComponentProps = {
	users: User[],
	onAddUser: Function,
	onRemoveUser: Function
}

export class UsersComponent extends React.Component<UsersComponentProps, void> {
	render(): JSX.Element {
		return (
			<div>
				<AddUserComponent onAddUser={this.props.onAddUser}/>
				<ul>
				{
					this.props.users.map((user: User) => {
						return <UserComponent key={user.id} id={user.id} name={user.name} onRemoveUser={this.props.onRemoveUser}/>
					})
				}
				</ul>
			</div>
		)
	}
}
