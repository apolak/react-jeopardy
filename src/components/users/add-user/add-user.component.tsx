import * as React from 'react';

type AddUserProps = {
	onAddUser: Function
}

export class AddUserComponent extends React.Component<AddUserProps, void> {
	userName: HTMLInputElement;

	render(): JSX.Element {
		return (
			<form onSubmit={ this.handleUserAdd.bind(this) }>
				<input ref={node => { this.userName = node}} placeholder="Fill username"/>
				<button type="submit">Add user</button>
			</form>
		)
	}

	private handleUserAdd(event: any) {
		event.preventDefault();

		if (!this.userName.value.trim()) {
			return;
		}

		this.props.onAddUser(this.userName.value);

		this.userName.value = '';
	}
}
