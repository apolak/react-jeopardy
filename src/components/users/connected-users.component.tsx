import { connect } from 'react-redux';
import { UsersComponent } from './users.component';
import { JoepardyState } from '../../model/domain';
import { createAddUserAction, createRemoveUserAction } from '../../model/actions';

const mapStateToProps: any = (state: JoepardyState): Object => {
	return {
		users: state.users
	}
};

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		onAddUser: (userName: string) => {
			dispatch(createAddUserAction(userName))
		},
		onRemoveUser: (userId: number) => {
			dispatch(createRemoveUserAction(userId));
		}
	}
};

export const ConnectedUsersComponent: React.ComponentClass<{}> = connect(mapStateToProps, mapDispatchToProps)(UsersComponent);
