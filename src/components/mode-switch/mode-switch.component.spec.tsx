import * as React from 'react';
import { mount } from "enzyme";
import { expect } from "chai";
import * as sinon from "sinon";
import { ModeSwitchComponent, GAME_MODE } from './mode-switch.component';
import { ADMIN_MODE } from './mode-switch.component';

describe('Mode switch', () => {
	it('should display game mode switch', () => {
		const props = {
			mode: ADMIN_MODE,
			onModeSwitch: () => {}
		};

		const wrapper = mount(<ModeSwitchComponent {...props} />);
		const body = wrapper.find('button');

		expect(body).to.have.text('Trigger ' + GAME_MODE +' Mode');
	});

	it('should display admin mode switch', () => {
		const props = {
			mode: GAME_MODE,
			onModeSwitch: () => {}
		};

		const wrapper = mount(<ModeSwitchComponent {...props} />);
		const body = wrapper.find('button');

		expect(body).to.have.text('Trigger ' + ADMIN_MODE +' Mode');
	});

	it('should dispatch mode change action with game mode when changing from admin mode', () => {
		const props = {
			mode: ADMIN_MODE,
			onModeSwitch: sinon.spy()
		};

		const wrapper = mount(<ModeSwitchComponent {...props} />);
		const body = wrapper.find('button');

		body.simulate('click');

		expect(props.onModeSwitch.withArgs(GAME_MODE).calledOnce).to.equal(true);
	});

	it('should dispatch mode change action with admin mode when changing from game mode', () => {
		const props = {
			mode: GAME_MODE,
			onModeSwitch: sinon.spy()
		};

		const wrapper = mount(<ModeSwitchComponent {...props} />);
		const body = wrapper.find('button');

		body.simulate('click');

		expect(props.onModeSwitch.withArgs(ADMIN_MODE).calledOnce).to.equal(true);
	});
});
