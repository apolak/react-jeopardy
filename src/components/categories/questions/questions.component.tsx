import * as React from 'react';
import { Question } from '../../../model/domain';
import { QuestionComponent } from '../question/question.component';

type QuestionsComponentProps = {
	questions: Question[],
	onRemoveQuestion: Function
};

export class QuestionsComponent extends React.Component<QuestionsComponentProps, void> {
	render(): JSX.Element {
		return (
			<ul>
				{
					this.props.questions.sort((a:Question, b:Question) => a.price - b.price).map((question: Question) => {
						return <QuestionComponent
							key={question.id}
							id={question.id}
							question={question.question}
							answer={question.answer}
							price={question.price}
						    onRemoveQuestion = {this.props.onRemoveQuestion}
						/>
					})
				}
			</ul>
		)
	}
}
