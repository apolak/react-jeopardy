import * as React from 'react';

type AddQuestionProps = {
	onAddQuestion: Function
}

export class AddQuestionComponent extends React.Component<AddQuestionProps, void> {
	question: HTMLInputElement;
	answer: HTMLInputElement;
	price: HTMLInputElement;

	render(): JSX.Element {
		return (
			<form onSubmit={ this.handleQuestionAdd.bind(this) }>
				<input ref={node => { this.question = node}} placeholder="Fill question"/>
				<input ref={node => { this.answer = node}} placeholder="Fill answer"/>
				<input ref={node => { this.price = node}} placeholder="Fill price"/>
				<button type="submit">Add question</button>
			</form>
		)
	}

	private handleQuestionAdd(event: any) {
		event.preventDefault();

		if (!this.question.value.trim() || !this.answer.value.trim() || !this.price.value.trim()) {
			return;
		}

		this.props.onAddQuestion(this.question.value, this.answer.value, this.price.value);

		this.question.value = '';
		this.answer.value = '';
		this.price.value = '';
	}
}
