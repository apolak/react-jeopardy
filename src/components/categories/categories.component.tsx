import * as React from 'react';
import { Category, Question } from '../../model/domain';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoryComponent } from './category/category.component';

export type CategoryItem = {
	category: Category,
	questions: Question[]
};

type CategoriesComponentProps = {
	categories: CategoryItem[],
	onAddCategory: Function,
	onRemoveCategory: Function,
	onAddQuestion: Function,
	onRemoveQuestion: Function
}

export class CategoriesComponent extends React.Component<CategoriesComponentProps, void> {
	render(): JSX.Element {
		return (
			<div>
				<AddCategoryComponent onAddCategory={this.props.onAddCategory}/>
				<div>
				{
					this.props.categories.map((item: CategoryItem) => {
						return <CategoryComponent
							key={item.category.id}
							id={item.category.id}
							name={item.category.name}
							questions={item.questions}
							onRemoveCategory={this.props.onRemoveCategory}
						    onAddQuestion={this.props.onAddQuestion}
						    onRemoveQuestion={this.props.onRemoveQuestion}
						/>
					})
				}
				</div>
			</div>
		)
	}
}
