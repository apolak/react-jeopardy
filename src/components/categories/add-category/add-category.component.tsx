import * as React from 'react';

type AddCategoryProps = {
	onAddCategory: Function
}

export class AddCategoryComponent extends React.Component<AddCategoryProps, void> {
	categoryName: HTMLInputElement;

	render(): JSX.Element {
		return (
			<form onSubmit={ this.handleCategoryAdd.bind(this) }>
				<input ref={node => { this.categoryName = node}} placeholder="Fill category name"/>
				<button type="submit">Add category</button>
			</form>
		)
	}

	private handleCategoryAdd(event: any) {
		event.preventDefault();

		if (!this.categoryName.value.trim()) {
			return;
		}

		this.props.onAddCategory(this.categoryName.value);

		this.categoryName.value = '';
	}
}
