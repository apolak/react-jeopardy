import * as React from 'react';

type QuestionComponentProps = {
	id: number,
	question: string,
	answer: string,
	price: number,
	onRemoveQuestion: Function
}

export class QuestionComponent extends React.Component<QuestionComponentProps, void> {
	render(): JSX.Element {
		return (
			<li onClick={this.handleRemoveQuestion.bind(this)}>{this.props.question} {this.props.answer} {this.props.price}</li>
		)
	}

	private handleRemoveQuestion() {
		if (confirm('Do you want to remove question?')) {
			this.props.onRemoveQuestion(this.props.id);
		}
	}
}
