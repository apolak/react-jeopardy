import * as React from 'react';
import { AddQuestionComponent } from '../add-question/add-question.component';
import { Question } from '../../../model/domain';
import { QuestionsComponent } from '../questions/questions.component';

type CategoryComponentProps = {
	id: number,
	name: string,
	questions: Question[],
	onRemoveCategory: Function,
	onAddQuestion: Function,
	onRemoveQuestion : Function
}

export class CategoryComponent extends React.Component<CategoryComponentProps, void> {
	render(): JSX.Element {
		return (
			<div>
				<p onClick={this.handleCategoryClick.bind(this)}>{this.props.name}</p>
				<AddQuestionComponent onAddQuestion={this.handleQuestionAdd.bind(this)}/>
				<QuestionsComponent questions={this.props.questions} onRemoveQuestion={this.props.onRemoveQuestion}/>
			</div>
		)
	}

	private handleCategoryClick() {
		if (confirm("Remove category ?")) {
			this.props.onRemoveCategory(this.props.id);
		}
	}

	private handleQuestionAdd(question: string, answer: string, price: number) {
		this.props.onAddQuestion(
			this.props.id,
			{
				question: question,
				answer: answer,
				price: price
			}
		);
	}
}
