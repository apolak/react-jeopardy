import * as React from 'react';
import { connect } from 'react-redux';
import { createResetGameAction } from '../../model/actions';

type ResetComponentProps = {
	onResetGame: Function
}

class ResetComponent extends React.Component<ResetComponentProps, void> {
	render(): JSX.Element {
		return (
			<button onClick={() => this.props.onResetGame()}>
				Reset Game
			</button>
		);
	}
}

const mapDispatchToProps: any = (dispatch: any): Object => {
	return {
		onResetGame: () => {
			dispatch(createResetGameAction());
		}
	}
};

export const ConnectedResetComponent: React.ComponentClass<{}> = connect(null, mapDispatchToProps)(ResetComponent);
