import { Reducer } from 'redux';
import * as React from 'react';
import { ADD_USER, REMOVE_USER, FAIL_QUESTION, ANSWER_QUESTION, RESET_GAME } from '../model/actions';

import update = require('immutability-helper');
import { findNextIndex } from '../utils/index.generator';
import { defaultState } from '../model/state';
import { User } from '../model/domain';

export const userReducer: Reducer<any> =  (state:any = [], action: any): any => {
	switch (action.type) {
		case RESET_GAME:
			return defaultState.users;
		case FAIL_QUESTION:
			return update(state, { [findUserIndex(state, action.userId)]: { score: {$set: state[findUserIndex(state, action.userId)].score - action.question.price}}});
		case ANSWER_QUESTION:
			return update(state, { [findUserIndex(state, action.userId)]: { score: {$set: state[findUserIndex(state, action.userId)].score + action.question.price}}});
		case ADD_USER:
			return update(state, {$push: [{
					id: findNextIndex(state),
					name: action.userName,
					score: 0
				}]});
		case REMOVE_USER:
			return update(state, {$splice: [[findUserIndex(state, action.id), 1]]});
		default:
			return state;
	}
};


const findUserIndex: Function = (state: User[], userId:number): number => {
	return state.findIndex((user:User) => user.id === userId);
};
