import { Reducer } from 'redux';
import * as React from 'react';
import {
	ADD_QUESTION, REMOVE_QUESTION, SKIP_QUESTION, ANSWER_QUESTION, FAIL_QUESTION,
	RESET_GAME
} from '../model/actions';

import update = require('immutability-helper');
import { findNextIndex } from '../utils/index.generator';
import { defaultState } from '../model/state';
import { Question } from '../model/domain';

export const questionsReducer: Reducer<any> =  (state:any = [], action: any): any => {
	let index: number = 0;
	switch (action.type) {
		case RESET_GAME:
			return defaultState.questions;
		case FAIL_QUESTION:
		case ANSWER_QUESTION:
			return update(state, { [state.findIndex((question:Question) => question.id === action.question.id)]: { answered: {$set: true}}});
		case SKIP_QUESTION:
			return update(state, { [state.findIndex((question:Question) => question.id === action.questionId)]: { answered: {$set: true}}});
		case ADD_QUESTION:
			return update(state, { $push: [{
				id: findNextIndex(state),
				question: action.question.question,
				answer: action.question.answer,
				price: action.question.price,
				answered: false,
				categoryId: action.categoryId
			}]});
		case REMOVE_QUESTION:
			for (let i=0; i<state.length; i++) {
				if (state[i].id === action.questionId) {
					index = i;
					break;
				}
			}

			return update(state, {$splice: [[index, 1]]});
		default:
			return state;
	}
};


