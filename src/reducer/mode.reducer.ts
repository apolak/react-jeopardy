import { Reducer } from 'redux';
import * as React from 'react';
import { CHANGE_MODE, RESET_GAME } from '../model/actions';
import update = require('immutability-helper');
import { ADMIN_MODE } from '../components/mode-switch/mode-switch.component';
import { defaultState } from '../model/state';

export const modeReducer: Reducer<any> =  (state:any = ADMIN_MODE, action: any): any => {
	switch (action.type) {
		case RESET_GAME:
			return defaultState.mode;
		case CHANGE_MODE:
			return action.targetMode;
		default:
			return state;
	}
};
