import { Reducer } from 'redux';
import * as React from 'react';
import {
	SELECT_QUESTION, NEXT_QUESTION, SKIP_QUESTION, ANSWER_QUESTION, FAIL_QUESTION,
	RESET_GAME
} from '../model/actions';

import update = require('immutability-helper');

export const activeQuestionReducer: Reducer<any> =  (state:any = [], action: any): any => {
	switch (action.type) {
		case RESET_GAME:
		case NEXT_QUESTION:
			return null;
		case ANSWER_QUESTION:
		case SKIP_QUESTION:
			return update(state, { answered: { $set: true} });
		case SELECT_QUESTION:
			return action.question;
		default:
			return state;
	}
};
