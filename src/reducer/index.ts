import { Reducer, combineReducers } from 'redux';
import { userReducer } from './users.reducer';
import { modeReducer } from './mode.reducer';
import { categoriesReducer } from './categories.reducer';
import { questionsReducer } from './questions.reducer';
import { activeQuestionReducer } from './active-question.reducer';

export const reducer: Reducer<any> = combineReducers({
	users: userReducer,
	mode: modeReducer,
	categories: categoriesReducer,
	questions: questionsReducer,
	activeQuestion: activeQuestionReducer
});
