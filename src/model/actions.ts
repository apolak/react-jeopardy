import { Question } from './domain';
export const ADD_QUESTION: string = 'ADD_QUESTION';
export const SKIP_QUESTION: string = 'SKIP_QUESTION';
export const NEXT_QUESTION: string = 'NEXT_QUESTION';
export const ANSWER_QUESTION: string = 'ANSWER_QUESTION';
export const FAIL_QUESTION: string = 'FAIL_QUESTION;'
export const SELECT_QUESTION: string = 'SELECT_QUESTION';
export const REMOVE_QUESTION: string = 'REMOVE_QUESTION';
export const EDIT_QUESTION: string = 'EDIT_QUESTION';

export const ADD_CATEGORY: string = 'ADD_CATEGORY';
export const REMOVE_CATEGORY: string = 'REMOVE_CATEGORY';
export const EDIT_CATEGORY: string = 'EDIT_CATEGORY';

export const CHANGE_MODE: string = 'CHANGE_MODE';

export const ADD_USER: string = 'ADD_USER';
export const REMOVE_USER: string = 'REMOVE_USER';
export const EDIT_USER: string = 'EDIT_USER';

export const RESET_GAME: string = 'RESET_GAME';

export const createChangeModeAction: Function = (targetMode: string): Object => {
	return {
		type: CHANGE_MODE,
		targetMode: targetMode
	}
};

export const createAddUserAction: Function = (userName: string): Object => {
	return {
		type: ADD_USER,
		userName: userName
	}
};

export const createRemoveUserAction: Function = (id: number): Object => {
	return {
		type: REMOVE_USER,
		id: id
	}
};

export const createAddCategoryAction: Function = (categoryName: string): Object => {
	return {
		type: ADD_CATEGORY,
		categoryName: categoryName
	}
};

export const createRemoveCategoryAction: Function = (id: number): Object => {
	return {
		type: REMOVE_CATEGORY,
		id: id
	}
};

export const createAddQuestionAction: Function = (categoryId: number, question: Object): Object => {
	return {
		type: ADD_QUESTION,
		categoryId: categoryId,
		question: question
	}
};

export const createRemoveQuestionAction: Function = (questionId: number): Object => {
	return {
		type: REMOVE_QUESTION,
		questionId: questionId
	}
};

export const createSelectQuestionAction: Function = (question: Question): Object => {
	return {
		type: SELECT_QUESTION,
		question: question
	}
};

export const createNextQuestionAction: Function = (): Object => {
	return {
		type: NEXT_QUESTION
	}
};

export const createSkipQuestionAction: Function = (questionId: number): Object => {
	return {
		type: SKIP_QUESTION,
		questionId: questionId
	}
};

export const createAnswerQuestionAction: Function = (question: Question, userId: number): Object => {
	return {
		type: ANSWER_QUESTION,
		question: question,
		userId: userId
	}
};

export const createFailQuestionAction: Function = (question: Question, userId: number): Object => {
	return {
		type: FAIL_QUESTION,
		question: question,
		userId: userId
	}
};

export const createResetGameAction: Function = () => {
	return {
		type: RESET_GAME
	}
};
