import { JoepardyState } from './domain';
import { GAME_MODE } from '../components/mode-switch/mode-switch.component';

export const defaultState: JoepardyState = {
	users: [
		{
			id: 1,
			name: 'Adam',
			score: 0
		},
		{
			id: 2,
			name: 'Tomasz',
			score: 0
		},
		{
			id: 3,
			name: 'Mateusz',
			score: 0
		},
		{
			id: 4,
			name: 'Marcin',
			score: 0
		}
	],
	categories: [
		{
			id: 1,
			name: 'Kat 1'
		},
		{
			id: 2,
			name: 'Kat 2'
		},
		{
			id: 3,
			name: 'Kat 3'
		},
		{
			id: 4,
			name: 'Kat 4'
		},
		{
			id: 5,
			name: 'Kat 5'
		}
	],
	questions: [
		{
			id: 1,
			question: 'q1',
			answer: 'a1',
			price: 100,
			answered: false,
			categoryId: 1
		},
		{
			id: 2,
			question: 'q1',
			answer: 'a1',
			price: 200,
			answered: false,
			categoryId: 1
		},
		{
			id: 3,
			question: 'q1',
			answer: 'a1',
			price: 300,
			answered: false,
			categoryId: 1
		},
		{
			id: 4,
			question: 'q1',
			answer: 'a1',
			price: 400,
			answered: false,
			categoryId: 1
		},
		{
			id: 5,
			question: 'q1',
			answer: 'a1',
			price: 500,
			answered: false,
			categoryId: 1
		},
		{
			id: 6,
			question: 'q1',
			answer: 'a1',
			price: 100,
			answered: false,
			categoryId: 2
		},
		{
			id: 7,
			question: 'q1',
			answer: 'a1',
			price: 200,
			answered: false,
			categoryId: 2
		},
		{
			id: 8,
			question: 'q1',
			answer: 'a1',
			price: 300,
			answered: false,
			categoryId: 2
		},
		{
			id: 9,
			question: 'q1',
			answer: 'a1',
			price: 400,
			answered: false,
			categoryId: 2
		},
		{
			id: 10,
			question: 'q1',
			answer: 'a1',
			price: 500,
			answered: false,
			categoryId: 2
		},
		{
			id: 11,
			question: 'q1',
			answer: 'a1',
			price: 100,
			answered: false,
			categoryId: 3
		},
		{
			id: 12,
			question: 'q1',
			answer: 'a1',
			price: 200,
			answered: false,
			categoryId: 3
		},
		{
			id: 13,
			question: 'q1',
			answer: 'a1',
			price: 300,
			answered: false,
			categoryId: 3
		},
		{
			id: 14,
			question: 'q1',
			answer: 'a1',
			price: 400,
			answered: false,
			categoryId: 3
		},
		{
			id: 15,
			question: 'q1',
			answer: 'a1',
			price: 500,
			answered: false,
			categoryId: 3
		},
		{
			id: 16,
			question: 'q1',
			answer: 'a1',
			price: 100,
			answered: false,
			categoryId: 4
		},
		{
			id: 17,
			question: 'q1',
			answer: 'a1',
			price: 200,
			answered: false,
			categoryId: 4
		},
		{
			id: 18,
			question: 'q1',
			answer: 'a1',
			price: 300,
			answered: false,
			categoryId: 4
		},
		{
			id: 19,
			question: 'q1',
			answer: 'a1',
			price: 400,
			answered: false,
			categoryId: 4
		},
		{
			id: 20,
			question: 'q1',
			answer: 'a1',
			price: 500,
			answered: false,
			categoryId: 4
		},
		{
			id: 21,
			question: 'q1',
			answer: 'a1',
			price: 100,
			answered: true,
			categoryId: 5
		},
		{
			id: 22,
			question: 'q1',
			answer: 'a1',
			price: 200,
			answered: false,
			categoryId: 5
		},
		{
			id: 23,
			question: 'q1',
			answer: 'a1',
			price: 300,
			answered: false,
			categoryId: 5
		},
		{
			id: 24,
			question: 'q1',
			answer: 'a1',
			price: 400,
			answered: false,
			categoryId: 5
		},
		{
			id: 25,
			question: 'q1',
			answer: 'a1',
			price: 500,
			answered: false,
			categoryId: 5
		}
	],
	mode: GAME_MODE,
	activeQuestion: null
};
