export type User = {
	id: number,
	name: string,
	score: number
}

export type Question = {
	id: number,
	price: number,
	question: string,
	answer: string,
	answered: boolean,
	categoryId: number
}

export type Category = {
	id: number,
	name: string
}

export type JoepardyState = {
	mode: string,
	users: User[],
	categories: Category[],
	questions: Question[],
	activeQuestion: Question
}
