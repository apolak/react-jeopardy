export const findNextIndex: Function = (collection: Array<any>): number => {
	if (collection.length > 0) {
		const sorted: Array<any> = collection.sort((a: any, b: any): any => {
			return a.id - b.id
		});

		return sorted[sorted.length-1].id + 1;
	}

	return 1;
};
