import * as React from 'react';
import * as ReactDom from 'react-dom';
import AppComponent from './components/app/app.component';
import { createStore, applyMiddleware } from 'redux';
import { Provider} from 'react-redux';
import { defaultState } from './model/state';
import { JoepardyState } from './model/domain';
import { loadState } from './service/local-storage.service';
import { localStorageMiddleware } from './middleware/local-storage.middleware';
import { reducer } from './reducer/index';

const localStorageState: JoepardyState = loadState();
const appInitialState: JoepardyState = (localStorageState) ? localStorageState : defaultState;

const store: any = createStore(
	reducer,
	appInitialState,
	applyMiddleware(localStorageMiddleware)
);

ReactDom.render(
	<Provider store={store}>
		<AppComponent/>
	</Provider>,
	document.getElementById('example')
);
