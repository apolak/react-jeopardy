var chai = require('chai');
var chaiEnzyme = require('chai-enzyme');
var chaiJsx = require('chai-jsx');
var jsdom = require('jsdom').jsdom;

chai.use(chaiEnzyme());
chai.use(chaiJsx);

global.document = jsdom('<!doctype html><html><body></body></html>');
global.window = document.defaultView;
global.navigator = global.window.navigator;
